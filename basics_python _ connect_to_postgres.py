#==============================================================================
''' 
python code to connect to a postgres server 
''' 
#==============================================================================

# library for connecting python to postgres
import psycopg2


# connection to postgres 
conn = psycopg2.connect(host="localhost",database="db1", user="tester1", password='remitoudic')
# to set user and role: 
#https://www.digitalocean.com/community/tutorials/how-to-use-roles-and-manage-grant-permissions-in-postgresql-on-a-vps--2#how-to-define-privileges-upon-role-creation
# sudo -u postgres psql postgres
print (" well connected to postgres server ")


# set cursor 
cur = conn.cursor()
# excecute query 
cur.execute('''SELECT version(); ''')
# bring data to python
data= cur.fetchall()

print(data)

